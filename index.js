//Adding Button Element
var button = document.querySelector('#btn1');
var eventObserver = rxjs.fromEvent(button, 'click');
eventObserver.pipe(rxjs.throttleTime(5000)).subscribe((ev) => console.log(ev));

//Adding Input Element
var inputElem = document.querySelector('input');
var inputeventObserver = rxjs.fromEvent(inputElem, 'input');
inputeventObserver
    .pipe(
        //rxjs.map(e=>e.target.value), //or
        rxjs.pluck('target', 'value'), //target is property in event then specify the value
        rxjs.debounceTime(500),
        rxjs.distinctUntilChanged()
    )
    .subscribe({
        next: function (value) {
            console.log(value)
        }
    })

//Adding source array of elements
var arr$ = rxjs.of(1, 2, 3);
arr$.pipe(
    // rxjs.reduce((acc,cur)=>{
    //     return acc +=cur;
    // },0)
    //or
    rxjs.scan((acc, cur) => {
        return acc += cur;
    }, 0)
).subscribe({
    next: function (val) {
        console.log(val)
    }
});

//Merge 2 observables
var firstName = document.querySelector("#firstName");
var secondName = document.querySelector("#secondName");
var fullName = document.querySelector("#fullName");

var firstName$ = rxjs.fromEvent(firstName, 'input');
var secondName$ = rxjs.fromEvent(secondName, 'input');
firstName$.pipe(
    rxjs.mergeMap(
        ev1 => secondName$.pipe(rxjs.map(ev2 => ev1.target.value + ev2.target.value))
    )
).subscribe(
    val => fullName.textContent = val
)

//Adding Error/Complete functions for observer
var observable = rxjs.Observable.create(function(obs) {
    obs.next('Hello');
    obs.next('World');
    //obs.error('Something Wrong');
    obs.complete();
});
var observer = {
    next: function (val) {
        console.log('cur value is ' + val);
    },
    error: function (err) {
        console.log(err);
    },
    complete: function () {
        console.log('Completed observable');
    }
}
observable.subscribe(observer);

//Adding Interval observable + Filter Operator + unsubscribe
// var evenNumber$ = rxjs.interval(1000);
// var obsEvenNum = evenNumber$.pipe(
//     rxjs.filter(val => val % 2 === 0 )
// ).subscribe(val => console.log(val));
//
// window.setTimeout(function () {
//     obsEvenNum.unsubscribe();
// },10000)

//Adding SwitchMap Operator
// var ids$ = rxjs.interval(1000);
// var subsrcibeTillTen =  ids$.pipe(rxjs.filter(val => val !== 0),
//     rxjs.switchMap(val => rxjs.from(fetch(`https://jsonplaceholder.typicode.com/todos/${val}`).then(res=>res.json())))
//     ).subscribe(val=>console.log(val));
// setTimeout(()=>{
//     subsrcibeTillTen.unsubscribe();
// },6000)

//Adding Subject
var sub$ = new rxjs.Subject();
sub$.subscribe({
    next:(val=>console.log("from 1 observable=====" + val)),
    error:((err)=>console.log(err)),
    complete:(()=>console.log('Complete'))
})
sub$.subscribe({
    next:(val=>console.log("from 2 observable=====" +val)),
    error:((err)=>console.log(err)),
    complete:(()=>console.log('Complete'))
})
sub$.next(1111)
sub$.next(2222)
sub$.next(3333)
sub$.error('somthing wrong')